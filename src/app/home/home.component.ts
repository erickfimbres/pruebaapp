import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { QuoteService } from './quote.service';
import { Marker, Response } from '../interfaces/app.iterfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  zoom = 5;
  center!: google.maps.LatLngLiteral;
  markers: Marker[] = [];

  constructor(private quoteService: QuoteService) {}

  ngOnInit() {
    this.center = {
      lat: 22.410756824517314,
      lng: -102.67171621627527,
    };

    this.quoteService.getLatLon().subscribe((response) => {
      response.results.map((item: Response) => {
        this.addMarker(item);
      });
    });
  }
  addMarker(response: Response) {
    this.markers.push({
      position: {
        lat: Number(response.latitude),
        lng: Number(response.longitude),
      },
      label: {
        color: 'red',
        text: response.name,
      },
      title: 'Marker title ' + (this.markers.length + 1),
      info: 'Marker info ' + (this.markers.length + 1),
    });
  }
}
