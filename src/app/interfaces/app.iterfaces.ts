export interface Marker {
  position: {
    lat: number;
    lng: number;
  };
  label: {
    color: string;
    text: string;
  };
  title: string;
  info: string;
}

export interface Response {
  latitude: number;
  longitude: number;
  name: string;
}
